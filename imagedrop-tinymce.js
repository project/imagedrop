var win = window.opener ? window.opener : window.dialogArguments;
if (!win) win = top;
tinyMCE = win.tinyMCE;
richedit = ( typeof tinyMCE == 'object' && tinyMCE.configs.length > 0 );
function sendToEditor(n) {
	o = document.getElementById('img'+n);
	h = o.innerHTML.replace(new RegExp('^\\s*(.*?)\\s*$', ''), '$1'); // Trim
	h = h.replace(new RegExp(' (class|title|width|height|id|onclick|onmousedown)=([^\'"][^ ]*)( |/|>)', 'g'), ' $1="$2"$3'); // Enclose attribs in quotes
	h = h.replace(new RegExp(' (width|height)=".*?"', 'g'), ''); // Drop size constraints
	h = h.replace(new RegExp(' on(click|mousedown)="[^"]*"', 'g'), ''); // Drop menu events
	h = h.replace(new RegExp('<(/?)A', 'g'), '<$1a'); // Lowercase tagnames
	h = h.replace(new RegExp('<IMG', 'g'), '<img'); // Lowercase again
	h = h.replace(new RegExp('(<img .+?")>', 'g'), '$1 />'); // XHTML
	h = h.replace('nofollow','lightbox');
	if ( richedit )
		win.tinyMCE.execCommand('mceInsertContent', false, h);
	else
		win.edInsertContent(win.edCanvas, h);
}
